<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress2');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'WkYIui3_`E4JoljgqQLrTs6`SF.(8!L9-!g0<R:% R`GcSMZ]`nu}KX3s2u1D/sn');
define('SECURE_AUTH_KEY',  'T3MC)XN>Q@YIYj(pDcri>s<L-+mR.R6<.cZfx}@TC1k^0i|ULmD#CE$&n2^_wkE8');
define('LOGGED_IN_KEY',    '2b{:7vZ)hN`(lQ2,@{TD_:^g12l7..oz.Ob6yH$YN05Xc[j$l5)#|zDdg_hn_d/H');
define('NONCE_KEY',        'Rq<wIPAQJ-JDf2koCBJIabr}^}sb=W:wkM]DU.SOfYyp_80Zlv5S.Ue:YuY?uU||');
define('AUTH_SALT',        'N&`h-A5#aQgAmSgP;U84YjEMt%r:9xpMubLlIC*_`/R7rWfRFR?<r}`%b(W-SMWg');
define('SECURE_AUTH_SALT', 'b{PZ?r*4p lCTF+Oql2_X*aKKiET3HX+(qDTj5{@|([{bXoafbu`[8mYHRdq~FaU');
define('LOGGED_IN_SALT',   '!EjIv%e,Qad4x5x;n}jkaCyNb7FEmLQ[|2% sek/ln7{=o/V^_7X/!aZ5[fqFov1');
define('NONCE_SALT',       'D>=;@ZDjvFZfv9j`mi$q$)5!AuP>EjjRz<S5UxDfe o(veCb;PALb-e/N4Qq_vG_');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
