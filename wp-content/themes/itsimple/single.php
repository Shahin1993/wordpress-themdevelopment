<?php get_header(); ?>

<?php while(have_posts()):the_post(); ?>
    <div class="article">
        <h2><a href="<?php the_permalink(); ?>" ><?php the_title();  ?></a></h2>
        <p class="infopost">Posted <span class="date">on 11 sep 2018</span> by <a href="#">Admin</a> &nbsp;&nbsp;|&nbsp;&nbsp; Filed under <a href="#">templates</a>, <a href="#">internet</a> <a href="#" class="com"><span>11</span></a></p>
        <div class="clr"></div>
        <div class="img"><?php the_post_thumbnail(); ?></div>
        <div class="post_content">
            <?php the_content(); ?>
            <p class="spec"><a href="#" class="rm">Read more &raquo;</a></p>
        </div>
        <div class="clr"></div>
    </div>
<?php endwhile; ?>

<?php get_footer(); ?>
