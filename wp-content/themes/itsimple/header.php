<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link href="<?php bloginfo('template_directory'); ?> /css/style.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/css/coin-slider.css" />
    <script type="text/javascript" src="<?php bloginfo('template_directory'); ?> /js/cufon-yui.js"></script>
    <script type="text/javascript" src="<?php bloginfo('template_directory'); ?> /js/cufon-georgia.js"></script>
    <script type="text/javascript" src="<?php bloginfo('template_directory'); ?> /js/jquery-1.4.2.min.js"></script>
    <script type="text/javascript" src="<?php bloginfo('template_directory'); ?> /js/script.js"></script>
    <script type="text/javascript" src="<?php bloginfo('template_directory'); ?> /js/coin-slider.min.js"></script>

    <?php wp_head(); ?>
</head>


<body <?php body_class(); ?> >
<div class="main">
    <div class="header">
        <div class="logo">
            <h1><a href="index.php"><?php bloginfo('title'); ?></span></a></h1>
        </div>
        <div class="header_resize">
            <div class="menu_nav">
                <?php wp_nav_menu(array(
                    'theme_location'=>'main-menu'
                )); ?>
            </div>

            <div class="clr"></div>