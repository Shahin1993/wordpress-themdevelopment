<?php get_header(); ?>
      <div class="slider">
        <div id="coin-slider"> <a href="#"><img src="<?php echo esc_url(get_template_directory_uri()); ?> /images/slide1.jpg" width="960" height="360" alt="" /> </a> <a href="#"><img src="<?php bloginfo('template_directory'); ?> /images/slide2.jpg" width="960" height="360" alt="" /> </a> <a href="#"><img src="<?php bloginfo('template_directory'); ?> /images/slide3.jpg" width="960" height="360" alt="" /> </a> </div>
        <div class="clr"></div>
      </div>
      <div class="clr"></div>
    </div>
  </div>
  <div class="content">
    <div class="content_resize">
      <div class="mainbar">

          <?php while(have_posts()):the_post(); ?>
        <div class="article">
            <h2><a href="<?php the_permalink(); ?>" ><?php the_title();  ?></a></h2>
          <p class="infopost">Posted <span class="date">on 11 sep 2018</span> by <a href="#">Admin</a> &nbsp;&nbsp;|&nbsp;&nbsp; Filed under <a href="#">templates</a>, <a href="#">internet</a> <a href="#" class="com"><span>11</span></a></p>
          <div class="clr"></div>
          <div class="img"><?php the_post_thumbnail(); ?></div>
          <div class="post_content">
             <?php the_content(); ?>
            <p class="spec"><a href="#" class="rm">Read more &raquo;</a></p>
          </div>
          <div class="clr"></div>
        </div>
      <?php endwhile; ?>


        <p class="pages"><small>Page 1 of 2</small> <span>1</span> <a href="#">2</a> <a href="#">&raquo;</a></p>
      </div>
      <div class="sidebar">
        <div class="searchform">
          <form id="formsearch" name="formsearch" method="post" action="#">
            <span>
            <input name="editbox_search" class="editbox_search" id="editbox_search" maxlength="80" value="Search our ste:" type="text" />
            </span>
            <input name="button_search" src="<?php bloginfo('template_directory'); ?> /images/search.gif" class="button_search" type="image" />
          </form>
        </div>
        <div class="gadget">
          <h2 class="star"><span>Sidebar</span> Menu</h2>
          <div class="clr"></div>
          <ul class="sb_menu">
            <li><a href="#">Home</a></li>
            <li><a href="#">TemplateInfo</a></li>
            <li><a href="#">Style Demo</a></li>
            <li><a href="#">Blog</a></li>
            <li><a href="#">Archives</a></li>
            <li><a href="#">Web Templates</a></li>
          </ul>
        </div>

        <div class="gadget">
           <?php dynamic_sidebar('leftsidebar'); ?>
        </div>
      </div>
      <div class="clr"></div>
    </div>
  </div>
  <?php get_footer();?>
