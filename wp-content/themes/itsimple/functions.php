<?php
add_theme_support('title_tag');

add_theme_support('custom-header');

add_theme_support('custom-background');
add_theme_support('post-thumbnails');

//load textdomain
load_theme_textdomain('itsimple',get_template_directory_uri().'/languages');
//menu register
register_nav_menus(array(
    'main-menu'=> __('Main Menu','itsimple'),
    'footer-menu'=> __('Footer Menu', 'itsimple')
));

function newdesign_left_sidebar(){
    register_sidebar(array(
        'name'=>'Left Sidebar',
        'description'=>'you can add left sidebar widgets from here',
        'id'=>'leftsidebar',
        'before_title'=>'<h2>',
        'after_title'=>'</h2>',
        'before_widget'=>'',
        'after_widget'=>'',
    ));

    register_sidebar(array(
        'name'=>'Right Sidebar',
        'description'=>'you can add right sidebar widgets from here',
        'id'=>'rightsidebar'
    ));
}
add_action('widgets_init','newdesign_left_sidebar');

?>